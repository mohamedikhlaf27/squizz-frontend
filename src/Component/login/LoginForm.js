import React from 'react';
import './Login.css';
import {Button} from 'reactstrap';
import {FetchLogin} from "./Util";

import { FaRegEye } from 'react-icons/fa';
import { FaRegEyeSlash } from 'react-icons/fa';


export const LoginForm = ({loginRequested}) => {

    // eslint-disable-next-line
    const [canLogin, setCanLogin] = React.useState("");
    // eslint-disable-next-line
    const [loggedIn, setLoggedIn] = React.useState("");
    const [message, setMessage] = React.useState("");

    const handleSubmit = (event) => {
        FetchLogin(event, setCanLogin, setLoggedIn, setMessage, loginRequested).then(r => message);
    }

    const [passwordShown, setPasswordShown] = React.useState(false);

    const togglePasswordVisibility = () => {
        setPasswordShown(!passwordShown);
    };

        return(

            <form data-testid="login-form" eslint-disable-next-line className="sign-in" onSubmit={handleSubmit}>
                <div className="empty"> </div>
                <div className="container">
                    <div className="card card-container">
                        <div className="pb-2"><
                            h4 id="Title">Sign in</h4>
                        </div>


                        <input type="email" id="email" className="form-control" placeholder="Email address" data-hj-allow
                               required={true} autoFocus={true}/>

                        <div className="passIcon-login">
                            <input type={passwordShown ? "text" : "password"} id="password" className="form-control" placeholder="Password"
                                   required={true} data-hj-allow/>

                            <span onClick={togglePasswordVisibility} className="icon-login">
                                <span>
                                    {passwordShown ?
                                        <FaRegEyeSlash/>:
                                        <FaRegEye/>
                                    }
                                </span>
                            </span>
                        </div>

                        <div className="text-danger pb-2">{message}</div>

                        <Button id="login-btn" className="btn btn-lg btn-primary btn-block btn-sign-in" type="submit">Sign in </Button>
                        <div>
                            <p>Don't have a account yet?
                                <a  className="register" href={'/register'}> Register.</a>
                            </p>
                        </div>
                    </div>
                </div>
            </form>
        )
}
export default LoginForm;
