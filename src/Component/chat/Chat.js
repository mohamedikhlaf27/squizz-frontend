import React, {Component} from "react";
import SockJsClient from 'react-stomp';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import './Chat.css'
import Navbar from "../navbar/Navbar";

class chat extends Component {
     constructor(props) {
         super(props);
        this.state = {
            messages: [],
            typedMessage: "",
            name: localStorage.getItem("Username")
        }
         this.redirect = this.redirect.bind(this);
     }
    sendMessage = () => {
        this.clientRef.sendMessage('/app/user', JSON.stringify({
            name: this.state.name,
            message: this.state.typedMessage
        }));
    };

    displayMessages = () => {
        return (
            <div>
                {this.state.messages.map(msg => {
                    return (
                        <div>
                            {this.state.name === msg.name ?
                                <div>
                                    <p className="title1">{msg.name} : {msg.message} </p><br/>
                                </div> :
                                <div>
                                    <p className="title2">{msg.name} : {msg.message}</p><br/>
                                </div>
                            }
                        </div>)
                })}
            </div>
        );
    };

    redirect(){
        this.clientRef.disconnect();
        this.props.history.push("/");
    }

    // <ol> toevoegen aan create
    render() {
        return (<>
            <Navbar/>
            <div>
                <div className="align-center">
                    <br/><br/>
                </div>
                <div className="align-center">
                    User: &nbsp; <p className="title1"> { this.state.name}</p>
                </div>

                <div className="card card-container">
                    {this.displayMessages()}
                </div>
                <div className="align-center pt-3">
                    <br/><br/>
                    <table>
                        <tr>
                            <td>
                                <TextField id="outlined-basic test-field" label="Enter Message to Send" variant="outlined"
                                           onChange={(event) => {
                                               this.setState({typedMessage: event.target.value});
                                           }}/>
                            </td>
                            <td>
                                <Button variant="contained" className="btn btn-lg btn-primary btn-block btn-send"
                                        onClick={this.sendMessage}>Send</Button>
                            </td>
                        </tr>
                    </table>
                </div>
                <br/><br/>

                <SockJsClient url='http://localhost:8080/websocket/'
                              topics={['/topic/chat']}
                              onConnect={() => {
                                  console.log("connected");
                              }}
                              onDisconnect={() => {
                                  console.log("Disconnected");
                              }}
                              onMessage={(msg) => {
                                  let jobs = this.state.messages;
                                  jobs.push(msg);
                                  this.setState({messages: jobs});
                                  console.log(this.state);
                              }}
                              ref={(client) => {
                                  this.clientRef = client
                              }}/>

                <Button variant="contained" className="btn btn-lg btn-primary btn-block btn-leave"
                        onClick={this.redirect}>Leave</Button>
            </div>

            </>
        )
    }
}

export default chat;